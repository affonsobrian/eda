# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from prettytable import *
from openpyxl import *

from itertools import permutations           
from itertools import combinations
from datetime import *

from django.db import models

# Create your models here.
class Prateleira(models.Model):
    identificador = models.IntegerField()
    descricao = models.CharField(max_length = 200)
    
    # Le a planilha e preenche o banco de dados com essas iformacoes
    def loadValuesFromXLSX(self, caminho = 'C:/Users/Brian/Desktop/eda/PLANILHA/ENTRADA.xlsx'):
        planilha = load_workbook(filename = caminho, read_only = True)
        aba = planilha['Prateleiras']
        for linha in aba:
            aux = []
            for celula in linha:
                aux.append(celula.value)
            try:
                p = Prateleira.objects.get(identificador = aux[0], descricao = aux[1])      
            except:
                try:
                    p = Prateleira(identificador = aux[0], descricao = aux[1])
                    p.save()
                    print 'LOG: Prateleira criada com sucesso!'
                except:
                    pass


    def __str__(self):
        return 'prateleira {} - {}'.format(self.identificador, self.descricao)


class Aresta(models.Model):
    entrada = models.ForeignKey(Prateleira, related_name="prateleira_entrada")
    saida = models.ForeignKey(Prateleira, related_name="prateleira_saida")
    distancia = models.FloatField()

    def loadValuesFromXLSX(self, caminho = 'C:/Users/Brian/Desktop/eda/PLANILHA/ENTRADA.xlsx'):
        planilha = load_workbook(filename = caminho, read_only = True)
        aba = planilha['Distancia']
        for linha in aba:
            aux = []
            for celula in linha:
                aux.append(celula.value)
            
            # Converte as entradas
            try:
                entrada = aux[0]
                saida = aux[1]
                entrada = Prateleira.objects.get(identificador = aux[0])
                saida = Prateleira.objects.get(identificador = aux[1])
                distancia = aux[2]
            except:
                pass

            # Verifica se existe uma aresta com aqueles dados, se nao existe cria
            try:
                a = Aresta.objects.get(entrada = entrada, saida = saida, distancia = distancia)     
            except:
                try:
                    a = Aresta(entrada = entrada, saida = saida, distancia = distancia)
                    a.save()
                    a = Aresta(entrada = saida, saida = entrada, distancia = distancia)
                    a.save()
                    print 'LOG: Aresta criada com sucesso!'
                except:
                    pass

    def __str__(self):
        #return 'From: {}, To: {}, Distancia: {}'.format(self.entrada, self.saida, self.distancia)
        return '{}, {}'.format(self.entrada,self.saida)


class Categoria(models.Model):
    nome = models.CharField(max_length = 200)
    prateleira = models.ForeignKey(Prateleira)

    def loadValuesFromXLSX(self, caminho = 'C:/Users/Brian/Desktop/eda/PLANILHA/ENTRADA.xlsx'):
        planilha = load_workbook(filename = caminho, read_only = True)
        aba = planilha['Prateleira_Categoria']
        for linha in aba:
            aux = []
            for celula in linha:
                aux.append(celula.value)
            # Converte as entradas
            try:
                prateleira = Prateleira.objects.get(identificador = aux[0])
                nome = aux[1]
            except:
                pass

            # Verifica se existe uma aresta com aqueles dados, se nao existe cria
            try:
                c = Categoria.objects.get(nome = nome, prateleira = prateleira)     
            except:
                try:
                    c = Categoria(nome = nome, prateleira = prateleira)
                    c.save()
                    print 'LOG: Categoria salva com sucesso!'
                except:
                    pass

    def __str__(self):
        return '{}'.format(self.nome)

class Produto(models.Model):
    nome = models.CharField(max_length = 200)
    categoria = models.ForeignKey(Categoria)

    def loadValuesFromXLSX(self, caminho = 'C:/Users/Brian/Desktop/eda/PLANILHA/ENTRADA.xlsx'):
        planilha = load_workbook(filename = caminho, read_only = True)
        aba = planilha['Produto_Categoria']
        for linha in aba:
            aux = []
            for celula in linha:
                aux.append(celula.value)
            # Converte as entradas
            try:
                nome = aux[0]
                categoria = Categoria.objects.get(nome = aux[1])
            except:
                pass
            # Verifica se existe uma aresta com aqueles dados, se nao existe cria
            try:
                p = Produto.objects.get(nome = nome, categoria = categoria)     
            except:
                try:
                    p = Produto(nome = nome, categoria = categoria)
                    p.save()
                    print 'LOG: Produto salvo com sucesso!'
                except:
                    pass
    def __str__(self):
        return '{}'.format(self.nome)

class CaminhoValido(models.Model):
    caminho = models.CharField(max_length = 120)
    peso = models.FloatField()

    def __str__(self):
        return 'Caminho: {}, Custo: {}'.format(self.caminho, self.peso)

    def __cmp__(self, other):
        return cmp(self.peso, other.peso)


    def GerarPermutacoesValidas(self, quantidade_nos):
        saida = []
        inicio = datetime.now()
        myList = range(1,quantidade_nos+1)
        sizeList=len(myList)
        allCombinations=[]
        cont = 0
        print 'Calculando caminhos'
        for theSize in range(1,sizeList+1):
            for combination in permutations(myList,theSize):
                peso = self.VerificarValidade(combination)
                if(peso != -1):
                    cv = CaminhoValido(caminho = self.ListToString(combination), peso = peso)
                    cv.save()
                    #saida.append(cv)
            print 'Caminhos de tamanhao {} calculados!'.format(theSize)
        print 'Todos os caminhos validos foram calculados com sucesso!'
        #return saida

    def GravarCaminhosValidos(self, caminhos):
        print 'Gravando caminhos'
        for caminho in caminhos:
            caminho.save()
        print 'Caminhos gravados com Sucesso'

    def VerificarValidade(self, nodes):
        peso = 0
        if(len(nodes) == 1):
            return 0
        elif(nodes[0] not in [1,2,7,8]):
            return -1
        for i in range(len(nodes)):
            try:
                if(nodes[i] != nodes[-1]):
                    entrada = Prateleira.objects.get(identificador = nodes[i])
                    saida = Prateleira.objects.get(identificador = nodes[i+1])
                    aresta = Aresta.objects.get(entrada = entrada, saida = saida)
                    peso = peso + aresta.distancia
            except:
                return -1
        return peso

    def FiltarCaminhosPorNos(self, list_nodes):
        list_nodes = ((str(list_nodes)).strip('[]')).split(', ')
        caminhos = list(CaminhoValido.objects.all())
        saida = []
        for caminho in caminhos:
            if(self.ChecarStringNoCaminho(list_nodes,caminho)):
                saida.append(caminho)
        saida.sort()
        return saida[0]

    def ChecarStringNoCaminho(self, list_nodes, caminho):
        for node in list_nodes:
            if(node not in caminho.caminho):
                return False
        return True

    def ListToString(self, lista):
        lista = str(lista)
        return lista.strip('()')

    