# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from mercado.models import *


class ArestaAdmin(admin.ModelAdmin):
    fields = ['entrada', 'saida','distancia']
    list_display =  ('entrada', 'saida','distancia')
    search_fields = (['entrada__identificador'])

# Register your models here.
admin.site.register(Prateleira)
admin.site.register(Categoria)
admin.site.register(Produto)
admin.site.register(Aresta, ArestaAdmin)
admin.site.register(CaminhoValido)
