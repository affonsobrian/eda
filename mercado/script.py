from mercado.models import *
from mercado.grafo.grafo import *
from random import choice
import os

def popularBanco(): 
    Prateleira.objects.all().delete()
    CaminhoValido.objects.all().delete()

    p = Prateleira()
    p.loadValuesFromXLSX()

    a = Aresta()
    a.loadValuesFromXLSX()

    c = Categoria()
    c.loadValuesFromXLSX()

    p = Produto()
    p.loadValuesFromXLSX()

'''
#c = CaminhoValido()
#caminhos = c.GerarPermutacoesValidas(len(Prateleira.objects.all()))
#c.GerarPermutacoesValidas(len(Prateleira.objects.all()))
#c.GravarCaminhosValidos(caminhos)
'''

def exibirMenuPrincipal():
    os.system('cls' if os.name == 'nt' else 'clear')
    while(True):
        print 'MENU PRINCIPAL'
        print '1 - ESCOLHER PRODUTOS'
        print '2 - EXIBIR CARRINHO'
        print '3 - LIMPAR CARRINHO'
        print '4 - MOSTRAR O CAMINHO'
        print 'Q - SAIR'
        entrada = str(raw_input('Escolha uma opcao: '))
        if(entrada.upper() in ['1','2','3','4','Q']):
            return entrada.upper()
        else:
            os.system('cls' if os.name == 'nt' else 'clear')
            print 'Entrada invalida, tente novamente!'

def exibirMenuProdutos():
    os.system('cls' if os.name == 'nt' else 'clear')
    while(True):
        print 'MENU PRODUTOS'
        print '1 - VER PRODUTOS POR CATEGORIA'
        print 'Q - RETORNAR AO MENU PRINCIPAL'
        entrada = str(raw_input('Escolha uma opcao: '))
        if(entrada.upper() in ['1','Q']):
            return entrada.upper()
        else:
            os.system('cls' if os.name == 'nt' else 'clear')
            print 'Entrada invalida, tente novamente!'


def exibirCategorias(caminho):
    categorias = list(Categoria.objects.all())
    pt_categorias = PrettyTable(['Codigo','Categoria'])
    for i in range(len(categorias)):
        pt_categorias.add_row([i,categorias[i]])
    while(True):
        print(pt_categorias)
        entrada = raw_input('Escolha uma categoria: ')
        if(entrada == 'Q' or entrada == 'q'):
            return
        elif(int(entrada) in range(len(categorias))):
            return categorias[int(entrada)]
        else:
            os.system('cls' if os.name == 'nt' else 'clear')
            print 'Entrada invalida, tente novamente!'


def escolherProdutos(prod, caminho):
    produtos =  []
    pt_produtos = PrettyTable(['Codigo','Produto'])
    entrada = exibirMenuProdutos()
    if(entrada == '1'):
        categoria = exibirCategorias(caminho)
        produtos = list(Produto.objects.filter(categoria = categoria))
        pt_produtos = PrettyTable(['Codigo','Produto'])
        for i in range(len(produtos)):
            pt_produtos.add_row([i,produtos[i]])
        while(True):
            print pt_produtos
            ent_produto = raw_input('Escolha um produto: ')

            if(ent_produto.upper() == 'Q'):
                return
            
            elif(ent_produto != '' and int(ent_produto) in range(len(produtos))):
                # se o produto nao estiver no carrinho ainda, adiciona
                if produtos[int(ent_produto)] not in prod:
                    prod.append(produtos[int(ent_produto)])
                    print '{} adicionado ao carrinho!'.format(prod[-1])
                for p in prod:
                    print p
                os.system('cls' if os.name == 'nt' else 'clear')

            else:
                os.system('cls' if os.name == 'nt' else 'clear')
                print 'Entrada invalida, tente novamente!'

def exibirCarrinho(prod):
    if(len(prod)!=0):
        print '------------------------------'
        print 'ITENS NO CARRINHO'
        print '------------------------------'
        for p in prod:
            print '- {}'.format(p)
        print '------------------------------'
    else:
        print 'Seu carrinho esta vazio' 
    raw_input('Pressione enter para Continuar')

def encontrarRota(prod, caminho):
    
    caminho.append(0)
    for p in prod:
        if(p.categoria.prateleira.identificador not in caminho):
            caminho.append(p.categoria.prateleira.identificador)

    g = Graph()
    g.pegarNodes(list(Aresta.objects.all()))
    print 'pq?'
    cam = g.EscolherMelhorPermutacao(caminho)
    
    exibirRota(prod, cam)

    raw_input('Pressione enter para Continuar')

def frasesMovimento():
    frases = ['direcione se a','dirija-se ate','va ate','siga ate','va para','dirja-se a','siga para','locomova se a']
    return choice(frases)

def exibirRota(prod, caminho):
    if(len(prod) != 0):
        #cam = converterRotaStringToInt(caminho)
        
        print 'CAMINHO: {}'.format(caminho['CAMINHO'])

        for c in caminho['CAMINHO']:
            print '--------------------------------------'
            prateleira = Prateleira.objects.get(identificador = c)
            print '\n{} {}\n'.format(frasesMovimento(),prateleira)
            exibirProdutos(prod, prateleira)
            print '--------------------------------------'
        print '\nDistancia percorrida: {} metros'.format(caminho['PESO'])
        print '--------------------------------------'
    else:
        print 'Parece que temos um bagunceiro aqui, nao?'

def converterRotaStringToInt(caminho):
    #converte a rota
    cam = caminho.caminho.split(',')
    #print cam
    for i in range(len(cam)):
        try:
            cam[i] = int(cam[i])
        except:
            cam.remove(cam[i])
    return cam

def exibirProdutos(prod, prateleira):
    produtos = []
    for p in prod:
        if(p.categoria.prateleira.identificador == prateleira.identificador):
            produtos.append(p)
    if(len(produtos)!=0):
        print 'Pegue os seguintes produtos nesta prateleira: '
        for p in produtos:
            print '- {}'.format(p)


def rotina():
    prod = []
    caminho = []
    while(True):
        entrada = exibirMenuPrincipal()
        if(entrada == '1'):
            escolherProdutos(prod, caminho)
                
        elif(entrada == '2'):
            exibirCarrinho(prod)

        elif(entrada == '3'):
            prod = []
            caminho = []
        elif(entrada == '4'):
            encontrarRota(prod, caminho)
        else:
            print 'Obrigado por utilizar este software!'
            return

#rotina()