# -*- coding: utf-8 -*-
from mercado.models import *

class Node:
    # Construtor
    def __init__(self, name, info = None, parent = None, ):
        self.name = name
        self.info = info
        self.parent = parent
        self.edges = []
        self.visited = False

    # Verifica se os nos sao iguais
    def __eq__(self, other):
        if other == None:
            return False
        return (self.name == other.name)
    
    # Modo como o objeto sera exibido no console
    def __str__(self):
        return 'name = {}, info = {}'.format(self.name, self.info)

    def AddEdge(self, nto, cost = 0):
        edge  = Edge(self,nto,cost)
        self.edges.append(edge)
        return edge

class Edge:
    # Construtor
    def __init__(self, nfrom, nto, distance):
        self.nfrom = nfrom
        self.nto = nto
        self.distance = distance
        self.visited = False

    # Verifica se as arestas sao iguais
    def __eq__(self, other):
        return (self.nfrom == other.nfrom and self.nto == other.nto and self.distance == other.distance)

    def __str__(self):
        return 'From = {}, To = {}, Distance = {}'.format(self.nfrom.name, self.nto.name, self.distance)

class Graph:
    def __init__(self):
        self.nodes = []
        self.edges = []
    
    def pegarNodes(self, edges_lista_busca = list(Aresta.objects.all())):
        for e in edges_lista_busca:
            # transforma os valores em nodes
            ne = Node(e.entrada.identificador)
            ns = Node(e.saida.identificador)

            # verifica se os nodes ja existem ou nao
            # se ja existem, referencia o objeto da lista
            # senao cria um novo e adiciona na lista
            if ne not in self.nodes:
                self.nodes.append(ne)
            else:
               ne = self.nodes[self.nodes.index(ne)]

            if ns not in self.nodes:
                self.nodes.append(ns)
            else:
               ns = self.nodes[self.nodes.index(ns)]

            edge = Edge(ne,ns,e.distancia)
            
            if edge not in ne.edges:
                ne.edges.append(edge)
                self.edges.append(edge)

    # funcao procurar node
    def Find(self, node_name):
        for node in self.nodes:
            if node_name == node.name:
                return node
        #print type(node_name)
        #print 'NAO EXISTE {}'.format(node_name)
        return None


    def AddNode(self, str_name, info = None):
        if str_name != None:
            n = Node(str_name, info)
            self.nodes.append(n)
        else:
            raise Exception('Nome Invalido')

    def AddEdge(self, nameFrom, nameTo, cost):
        nfrom = self.Find(nameFrom)
        nto = self.Find(nameTo)
        if nfrom != None and nto != None:
            edge = nfrom.AddEdge(nto, cost) 
            self.edges.append(edge)

    # begin e end sao stirngs
    def GenerateSearchTree(self, begin):#, end):
        graph = Graph()
        #nodes = []
        nfrom = []
        nfrom = self.Find(begin)
        #nto = self.Find(end)
        if nfrom == None: # or nto == None:
            raise Exception('Node nulo')
        #nodes.append(nfrom)
        #nfrom.visited = True 
        graph.AddNode(begin, 0)

        for n in self.nodes:
            n.visited = False
        nfrom.visited = True

        while(len(graph.nodes) != len(self.nodes)):
            mini = -1
            mininame = '123'
            parent = '123'

            for n in graph.nodes:
                node = self.Find(n.name)
                for e in node.edges:
                    if e.nto.visited == False:
                        if e.distance + n.info < mini  or mini == -1:
                            mini = e.distance + n.info
                            mininame = e.nto.name
                            parent = e.nfrom.name
            
            #print '{} -> {} mini = {}'.format(parent,mininame,mini)
            
            #if(mininame != None)
            #print mininame
            graph.AddNode(mininame, mini) # + graph.Find(parent).info)
            graph.AddEdge(parent, mininame, mini)
            miniNode = graph.Find(mininame)
            miniParent = graph.Find(parent)
            #miniNode.visited = True
            self.Find(mininame).visited = True
            miniNode.parent = miniParent
            #nodes.append(miniNode)
            
        return graph

    def GerarPermutacoesValidas(self, myList):
        saida = []
        for combination in permutations(myList,len(myList)):
            if(combination[0] == 0):
                saida.append(combination)
        return saida

    def EscolherMelhorPermutacao(self, myList):
        permutacoes = self.GerarPermutacoesValidas(myList)
        dicionario = self.Dijkstra(myList)
        melhor_permutacao = None
        melhor_soma = 0
        saida = {}

        for p in permutacoes:
            soma = 0
            for i in range(len(p)-1):
                soma += dicionario[(p[i],p[i+1])]['PESO']
            
            if(melhor_permutacao == None or melhor_distancia > soma):
                melhor_distancia = soma
                melhor_permutacao = p

        caminho = []
        for i in range(len(melhor_permutacao)-1):
            if len(caminho) != 0: 
                caminho.pop()
            caminho += dicionario[(melhor_permutacao[i],melhor_permutacao[i+1])]['CAMINHO']
        #print 'Caminho: {}, Distancia: {}'.format(caminho,melhor_distancia)
        saida['CAMINHO'] = caminho
        saida['PESO'] = melhor_distancia
        return saida 

    # begin e end sao strings
    def Dijkstra(self, nodes_inicio = []):
        saida = {}
        for i in range(len(nodes_inicio)):
            arvore_busca = self.GenerateSearchTree(nodes_inicio[i])
            for j in range(len(nodes_inicio)):
                if(i != j):
                    list_aux = []
                    peso_total = arvore_busca.Find(nodes_inicio[j]).info
                    node = arvore_busca.Find(nodes_inicio[j])
                    while(node != None):
                        list_aux.append(node.name)
                        node = node.parent
                    list_aux.reverse()
                    saida[(nodes_inicio[i],nodes_inicio[j])] = {}
                    saida[(nodes_inicio[i],nodes_inicio[j])]['PESO'] = peso_total
                    saida[(nodes_inicio[i],nodes_inicio[j])]['CAMINHO'] = list_aux
        return saida

